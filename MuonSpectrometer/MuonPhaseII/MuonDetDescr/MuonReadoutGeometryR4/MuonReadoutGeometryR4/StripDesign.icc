/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONREADOUTGEOMETRYR4_STRIPDESIGN_ICC
#define MUONREADOUTGEOMETRYR4_STRIPDESIGN_ICC

#define SET_DIRECTION(METHOD, DIRVEC, FROM, TO)                             \
     inline const Amg::Vector2D& StripDesign::METHOD() const {              \
        if (!DIRVEC) {                                                      \
             DIRVEC.set(std::make_unique<Amg::Vector2D>((TO-FROM).unit())); \
        }                                                                   \
        return (*DIRVEC);                                                   \
     }

namespace MuonGMR4 {
    inline const Amg::Vector2D& StripDesign::cornerBotLeft() const { return m_bottomLeft; }
    inline const Amg::Vector2D& StripDesign::cornerBotRight() const { return m_bottomRight; }
    inline const Amg::Vector2D& StripDesign::cornerTopLeft() const { return m_topLeft; }
    inline const Amg::Vector2D& StripDesign::cornerTopRight() const { return m_topRight; }

    SET_DIRECTION(edgeDirTop, m_dirTopEdge, m_topLeft, m_topRight)
    SET_DIRECTION(edgeDirBottom, m_dirBotEdge, m_bottomLeft, m_bottomRight)
    SET_DIRECTION(edgeDirLeft, m_dirLeftEdge, m_topLeft, m_bottomLeft)
    SET_DIRECTION(edgeDirRight, m_dirRightEdge, m_topRight, m_bottomRight)

    inline double StripDesign::lenTopEdge() const { return isFlipped() ? 2* longHalfHeight() :  m_lenSlopEdge; }
    inline double StripDesign::lenLeftEdge() const {return isFlipped() ? m_lenSlopEdge : 2* shortHalfHeight();}
    inline double StripDesign::lenBottomEdge() const { return isFlipped() ? 2* shortHalfHeight() : m_lenSlopEdge; }
    inline double StripDesign::lenRightEdge() const{ return isFlipped() ? m_lenSlopEdge  : 2 * longHalfHeight(); }

    inline double StripDesign::stripPitch() const { return m_stripPitch; }
    inline double StripDesign::stripWidth() const { return m_stripWidth; }
    inline double StripDesign::halfWidth() const { return m_halfX; }
    inline double StripDesign::shortHalfHeight() const { return m_shortHalfY; }
    inline double StripDesign::longHalfHeight() const { return m_longHalfY; }
    inline int StripDesign::numStrips() const { return m_numStrips; }
    inline bool StripDesign::hasStereoAngle() const { return m_hasStereo; }
    inline bool StripDesign::isFlipped() const { return m_isFlipped; }
    inline double StripDesign::stereoAngle() const {return m_stereoAngle; }
    inline int StripDesign::firstStripNumber() const { return m_channelShift; }
    inline Amg::Vector2D StripDesign::stripPosition(int stripCh) const {
        return m_firstStripPos + (1.*stripCh) * stripPitch() *  Amg::Vector2D::UnitX();
    }
    
    inline Amg::Vector2D StripDesign::leftInterSect(int stripCh, bool uncapped) const {
        /// Nominal strip position position
        return leftInterSect(stripPosition(stripCh), uncapped);       
    }
    inline Amg::Vector2D StripDesign::leftInterSect(const Amg::Vector2D& stripPos, bool uncapped) const {
        /// We expect lambda to be positive
        const std::optional<double> lambda = Amg::intersect<2>(stripPos, m_stripDir, cornerTopLeft(), edgeDirTop());
        if (!lambda) {
            ATH_MSG_WARNING("The strip "<<Amg::toString(stripPos, 2)
                          <<" does not intersect the top edge "<<Amg::toString(cornerTopLeft(), 2)
                          <<" + lambda "<<Amg::toString(edgeDirTop(), 2));
            return Amg::Vector2D::Zero();
        }
        /// If the channel is a stereo channel && lamda is either smaller 0 or longer
        /// then the bottom edge, then it's a routed strip
        if (!uncapped && ((*lambda) < 0  || (*lambda) > lenTopEdge())) {
            const bool pickLeft =  (*lambda) < 0;
            const Amg::Vector2D& edgePos{pickLeft ?  cornerTopLeft() : cornerTopRight()};
            const Amg::Vector2D& edgeDir{pickLeft ?  edgeDirLeft() : edgeDirRight()};
            const double edgeLen = pickLeft ? lenLeftEdge(): lenRightEdge();
            const std::optional<double> top_line = Amg::intersect<2>(stripPos, m_stripDir, edgePos, edgeDir);
            if (!top_line || (*top_line) <0. || (*top_line) > edgeLen) {
                ATH_MSG_WARNING(__func__<<"() The strip "<<Amg::toString(stripPos, 2)
                                <<" does not intersect with the edge "<<Amg::toString(edgePos));
                return Amg::Vector2D::Zero();
            }
            return edgePos + (*top_line) * edgeDir;
       }
       return cornerTopLeft() + (*lambda) * edgeDirTop();
    }
    inline Amg::Vector2D StripDesign::rightInterSect(const Amg::Vector2D& stripPos, bool uncapped) const {
        std::optional<double> lambda = Amg::intersect<2>(stripPos, m_stripDir, cornerBotLeft(), edgeDirBottom());
        if (!lambda) {
            ATH_MSG_WARNING("The strip "<<Amg::toString(stripPos,2)
                          <<" does not intersect the left edge "<<Amg::toString(cornerBotLeft(), 2)
                          <<" + lambda "<<Amg::toString(edgeDirBottom(), 2));
            return Amg::Vector2D::Zero();
        }
        /// If the channel is a stereo channel && lamda is either smaller 0 or longer
        /// then the bottom edge, then it's a routed strip
        if (!uncapped  && ( (*lambda) < 0 || (*lambda) > lenBottomEdge())) {
            const bool selLeftEdge = (*lambda) < 0; 
            const Amg::Vector2D& edgePos{selLeftEdge ? cornerBotLeft() : cornerBotRight()};
            const Amg::Vector2D& edgeDir{selLeftEdge ? edgeDirLeft() : edgeDirRight()};
            const double edgeLen{selLeftEdge ? lenLeftEdge() : lenRightEdge()};
            std::optional<double> bottom_line = Amg::intersect<2>(stripPos, m_stripDir, edgePos, edgeDir);            
            if (!bottom_line || (*bottom_line) > 0 || (*bottom_line) < -edgeLen) {
                ATH_MSG_WARNING("The strip "<<Amg::toString(stripPos, 2)<<" does not intersect with the left/right edges ");
                return Amg::Vector2D::Zero();
            }
            return edgePos + (*bottom_line) * edgeDir;            
        }
        return cornerBotLeft() + (*lambda) * edgeDirBottom();
    }
    //============================================================================
    inline double StripDesign::stripLength(int stripNumber) const {
        const int stripCh = (stripNumber - m_channelShift);
        if (stripCh < 0 ||  stripCh > numStrips()) {
            ATH_MSG_WARNING("center() -- Invalid strip number given "<<stripNumber<<" allowed range ["
                         <<m_channelShift<<";"<<numStrips()<<"]");
            return 0.;
        }
        const Amg::Vector2D strip{stripPosition(stripCh)};
        const Amg::Vector2D delta = rightInterSect(strip) - leftInterSect(strip);
        ATH_MSG_VERBOSE("Strip "<<stripNumber<<" right edge: "<<Amg::toString(rightInterSect(strip), 2)
                     <<" left edge: "<<Amg::toString(leftInterSect(strip),2)<<" length: "<<std::hypot(delta.x(), delta.y()));
        return std::hypot(delta.x(), delta.y());
    }
    inline Amg::Vector2D StripDesign::rightInterSect(int stripCh, bool uncapped) const {
        return rightInterSect(stripPosition(stripCh), uncapped);
    }
    
    inline Amg::Vector2D StripDesign::stripCenter(int stripCh) const {
       return 0.5 *(leftInterSect(stripCh) + rightInterSect(stripCh));      
    }

    inline std::optional<Amg::Vector2D> StripDesign::center(int stripNumber) const {
        const int stripCh = (stripNumber - m_channelShift);
        if (stripCh < 0 ||  stripCh > numStrips()) {
            ATH_MSG_WARNING("center() -- Invalid strip number given "<<stripNumber<<" allowed range ["
                         <<m_channelShift<<";"<<numStrips()<<"]");
            return std::nullopt;
        }
        return std::make_optional<Amg::Vector2D>(m_stereoRotMat * stripCenter(stripCh));
    }    
    inline std::optional<Amg::Vector2D> StripDesign::leftEdge(int stripNumber) const {
       const int stripCh = (stripNumber - m_channelShift);
       if (stripCh < 0 ||  stripCh > numStrips()) {
            ATH_MSG_WARNING("leftEdge() -- Invalid strip number given "<<stripNumber<<" allowed range ["
                          <<m_channelShift<<";"<<numStrips()<<"]");
            return std::nullopt;
       }
       return std::make_optional<Amg::Vector2D>(m_stereoRotMat * leftInterSect(stripCh));
    }
    inline std::optional<Amg::Vector2D> StripDesign::rightEdge(int stripNumber) const {
        const int stripCh = (stripNumber - m_channelShift);
        if (stripCh < 0 ||  stripCh > numStrips()) {
            ATH_MSG_WARNING("rightEdge() -- Invalid strip number given "<<stripNumber<<" allowed range ["
                          <<m_channelShift<<";"<<numStrips()<<"]");
            return std::nullopt;
        }
        return std::make_optional<Amg::Vector2D>(m_stereoRotMat * rightInterSect(stripCh));
    }
    inline double StripDesign::distanceToStrip(const Amg::Vector2D& pos, int stripNumber) const {
        std::optional<Amg::Vector2D> stripCent = center(stripNumber);
        if (!stripCent) return std::numeric_limits<double>::max();
        return (pos - (*stripCent)).x();
    }
    inline int StripDesign::stripNumber(const Amg::Vector2D& pos) const {
        const Amg::Vector2D posInNominal = m_nominalRotMat*pos;
        const double xMid = (posInNominal - m_firstStripPos).dot(m_stripNormal) / m_stripNormal.x();
        const int chNum = static_cast<int>(std::floor( xMid  / stripPitch()));
        if (chNum < 0 || chNum > numStrips()) {
            ATH_MSG_VERBOSE("Object "<<Amg::toString(pos)<<" is not covered by any strip."
                          <<" Virtual channel number: "<<chNum<<" max strips: "<<numStrips());
            return -1;
        }
        return chNum + m_channelShift;
    }
}
#undef SET_DIRECTION
#endif